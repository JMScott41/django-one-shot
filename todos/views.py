from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todo_list_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list_list,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": details,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        item_form = TodoItemForm(request.POST)
        if item_form.is_valid():
            new_item = item_form.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        item_form = TodoItemForm()

    context = {
        "item_form": item_form,
    }
    return render(request, "todos/create_item.html", context)


def todo_list_update(request, id):
    updateform = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        update_form = TodoListForm(request.POST, instance=updateform)
        if update_form.is_valid():
            update_form.save()
            return redirect("todo_list_detail", id=id)
    else:
        update_form = TodoListForm(instance=updateform)

    context = {
        "update_form": update_form,
    }
    return render(request, "todos/update.html", context)


def todo_item_update(request, id):
    task = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        update_item_form = TodoItemForm(request.POST, instance=task)
        if update_item_form.is_valid():
            new_item = update_item_form.save()
            return redirect("todo_list_detail", new_item.list.id)
    else:
        update_item_form = TodoItemForm(instance=task)

    context = {
        "update_item_form": update_item_form,
    }
    return render(request, "todos/todo_item.html", context)


def list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    context = {
        "delete_view": delete_list,
    }

    return render(request, "todos/delete.html", context)
